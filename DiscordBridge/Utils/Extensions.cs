﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Eco.Plugins.DiscordBridge
{
    static class Extension
    {
        public static bool CheckURLValid(this string source)
        {
            Uri uriResult;
            return Uri.TryCreate(source, UriKind.Absolute, out uriResult) && uriResult.Scheme == Uri.UriSchemeHttps;
        }

        public static string StripHTML(this string text)
        {
            text = text.Replace("<b>","**");
            text = text.Replace("</b>","**");
            return Regex.Replace(text, "<.*?>", String.Empty).Trim();
        }

        public static DiscordCommand GetFieldAttr(
            this Bot source,
            Expression<Func<Bot, object>> field)
        {
            var member = field.Body as MemberExpression;
            if (member == null) return null; // or throw exception

            var fieldName = member.Member.Name;

            var test = typeof(Bot);
            var fieldType = test.GetField(fieldName);
            if (fieldType != null)
            {
                var attribute = fieldType.GetCustomAttribute<DiscordCommand>();

                return attribute;
            }

            return null;
        }
    }

    public static class ReflectionExtensions
    {

        public static object InvokeWithNamedParameters(this MethodBase self, object obj, IDictionary<string, object> namedParameters)
        {
            return self.Invoke(obj, MapParameters(self, namedParameters));
        }

        public static object[] MapParameters(MethodBase method, IDictionary<string, object> namedParameters)
        {
            string[] paramNames = method.GetParameters().Select(p => p.Name).ToArray();
            object[] parameters = new object[paramNames.Length];
            for (int i = 0; i < parameters.Length; ++i)
            {
                parameters[i] = Type.Missing;
            }
            foreach (var item in namedParameters)
            {
                var paramName = item.Key;
                var paramIndex = Array.IndexOf(paramNames, paramName);
                parameters[paramIndex] = item.Value;
            }
            return parameters;
        }
    }

    static class Helper
    {
        public static string GetDLLFolder()
        {
            string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }
    }
}
