﻿using Eco.Shared.Utils;

namespace Eco.Plugins.DiscordBridge
{
    public class Logger
    {
        static string projectname = "DISCORDBRIDGE";
        public static void Debug(string message)
        {
            #if DEBUG
            Log.Write(projectname+" DEBUG: " + message + "\n");
            #endif
            
        }

        public static void Info(string message)
        {
            Log.Write(projectname+" INFO: " + message + "\n");
        }

        public static void Error(string message)
        {
            Log.Write(projectname+" ERROR: " + message + "\n");
        }

        public Logger()
        {
            projectname = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToUpper();
        }
    }
}