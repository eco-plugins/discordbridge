﻿/**
 * File: Mod.cs
 * Eco Version:
 * Mod Version:
 * 
 * Author:
 * 
 * 
 * Purpose of mod here.
 * 
 * 
 * Features:
 * 
 */

using Eco.Core;
using Eco.Core.Plugins;
using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;
using Eco.Shared.Utils;
using System.Threading;

namespace Eco.Plugins.DiscordBridge
{
    public class DiscordBridge : IModKitPlugin, IInitializablePlugin, IConfigurablePlugin
    {
        private PluginConfig<DiscordBridgeConfig> configOptions;
        private ChatNotifier chatNotifier;
        private ProfessionInName professionInName;
        private Simulation simulation;
        private Bot bot;
        private string _status = "No Connection Attempt Made";

        public DiscordBridge()
        {
            Logger.Info("Loaded Discordbridge");
            SetupConfig();
            configOptions = new PluginConfig<DiscordBridgeConfig>("DiscordBridge");
            
            professionInName = new ProfessionInName();
            chatNotifier = new ChatNotifier();
            bot = new Bot();
        }

        public void Initialize(TimedTask timer)
        {
            Logger.Info("Initializing Discordbridge");
            
            chatNotifier.Initialize();
            professionInName.Initialize();
            bot.SetupAsync();

            new Thread(() => { chatNotifier.Run(); })
            {
                Name = "ChatNotifierThread"
            }.Start();

            Logger.Info(Simulation.DiscordBridgeSimulation.GetEnviromentStatsString());
            _status = "Active";
        }

        public IPluginConfig PluginConfig
        {
            get { return configOptions; }
        }

        public DiscordBridgeConfig DiscordPluginConfig
        {
            get { return PluginConfig.GetConfig() as DiscordBridgeConfig; }
        }

        private void SetupConfig()
        {
            Logger.Debug("Setting up config");
            configOptions = new PluginConfig<DiscordBridgeConfig>("DiscordBridgeConfig");

            configOptions.Config.OnConfigChanged.Subscribe(x =>
            {
                Logger.Debug("Config changed: "+ x);
                SaveConfig();

            });
            SaveConfig();
        }

        protected void SaveConfig()
        {
            Logger.Debug("Saving Config");
            configOptions.Save();
        }

        public static DiscordBridge Obj
        {
            get { return PluginManager.GetPlugin<DiscordBridge>(); }
        }

        public object GetEditObject()
        {
            return configOptions.Config;
        }

        public void OnEditObjectChanged(object o, string param)
        {
            SaveConfig();
        }

        public string GetStatus()
        {
            return _status;
        }

        public string getSettingValue(string key)
        {
            return DiscordPluginConfig.GetPropertyByName<string>(key);
        }

    }
    
}
