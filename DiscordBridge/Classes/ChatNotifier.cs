﻿using Eco.Core.Utils;
using Eco.Gameplay.Systems.Chat;
using Eco.Shared.Services;
using Eco.Shared.Utils;
using Eco.Simulation.Time;
using System;
using System.IO;
using System.Threading;

namespace Eco.Plugins.DiscordBridge
{
    public class ChatNotifier
    {
        public ThreadSafeAction<ChatMessage> OnMessageReceived { get; set; } = new ThreadSafeAction<ChatMessage>();

        private DiscordWebhook discord;
        private double lastCheckTime = Double.MaxValue;
        private const int POLL_DELAY = 500;
        private readonly bool showCategories = false;


        public void Initialize()
        {
            Logger.Info("ChatNotifier.Initialize()");
            lastCheckTime = WorldTime.Seconds;
            discord = new DiscordWebhook(DiscordBridge.Obj.DiscordPluginConfig.WebhookUrl);
        }

        public void Run()
        {
            while (true)
            {
                var newMessages = ChatServer.GetPlayerMessages(lastCheckTime);
                newMessages.ForEach(message =>
                {
                    //Log.Write("Message found, invoking callback.");
                    if (!message.Priv)
                    {
                        var text = message.Text.StripHTML();

                        var msg = $"**{message.Sender}** ({message.Category}): {text}";
                        if (message.Sender.IsEmpty())
                        {
                            msg = $"{text}";
                        }else
                        if (!showCategories)
                        {
                            msg = $"**{message.Sender}**: {text}";
                        }

                        discord.SendDiscordMessage(msg);

                        //Logger.Info(msg);
                        // File.AppendAllText(DiscordBridge.Obj.DiscordPluginConfig.MessageLogPath, message.Text + Environment.NewLine);

                    }
                    OnMessageReceived.Invoke(message);
                });
                lastCheckTime = WorldTime.Seconds;
                Thread.Sleep(POLL_DELAY);
            }
        }
    }
  
}
