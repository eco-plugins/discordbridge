﻿using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Chat;
using Eco.Shared.Services;
using System;
using System.Runtime.CompilerServices;
using Eco.Shared.Utils;
using static Eco.Plugins.DiscordBridge.Simulation;

namespace Eco.Plugins.DiscordBridge
{
    class Commands
    {
        public class DiscordBridgeCommands : IChatCommandHandler
        {
            [ChatCommand("A command to broadcast a message", ChatAuthorizationLevel.Admin)]
            public static void Broadcast(User user, string message)
            {
                foreach (User u in UserManager.OnlineUsers)
                {
                    ChatManager.ServerMessageToPlayer($"[Broadcast] {String.Format(message, u.Name)}", u, false, DefaultChatTags.General, ChatCategory.Default);
                }
            }

            [ChatCommand("A command to broadcast a message in a UI for very important message.", ChatAuthorizationLevel.Admin)]
            public static void BroadcastUI(User user, string title, string message)
            {
                foreach (User u in UserManager.OnlineUsers)
                {
                    u.Player.OpenInfoPanel(title, String.Format(message, u.Name));
                    ChatManager.ServerMessageToPlayer($"[Broadcast] {String.Format(message, u.Name)}", u, false, DefaultChatTags.General, ChatCategory.Default);
                }
            }

            [ChatCommand("A command to broadcast a message in a UI for very important message.", ChatAuthorizationLevel.Admin)]
            public static void ResetNames(User user)
            {
                ProfessionInName professionInName = new ProfessionInName();
                professionInName.resetNames();
                ChatManager.ServerMessageToPlayer($"Names was resettet", user);
            }

            [ChatCommand("Enviroment report", ChatAuthorizationLevel.Admin)]
            public static void EnvReport(User user)
            {
                FormattableString stats = FormattableStringFactory.Create(DiscordBridgeSimulation.GetEnviromentStatsString());
                ChatManager.ServerMessageToPlayer(stats, user, true, DefaultChatTags.Notifications, ChatCategory.Info);
            }


            [ChatCommand("Get a list of users and their jobs", ChatAuthorizationLevel.User)]
            public static void JobReport(User currentUser) => Eco.Gameplay.Players.UserManager.Users.ForEach(user =>
            {
                if (user.Name == "") return;

                string userName = user.Name;
                string professions = GetHighestProfession(user);

                Logger.Info("Command.JobReport(): User: "+ user.Name);
                Logger.Info("Command.JobReport(): Profession(s): "+ professions);

                FormattableString text = $"{userName}: {professions}";
                ChatManager.ServerMessageToPlayer(text, user);
            });

            private static string GetHighestProfession(User user)
            {
                return new ProfessionInName().GetHighestProfession(user);
            }
        }
    }
}
