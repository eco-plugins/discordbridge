﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Chat;
using Eco.Shared.Utils;
using Eco.Simulation.Types;
using Eco.Stats;

using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;

//NDgxNTcxOTI1MjY1NjEyODEw.Dpj32w.59q3TJXhPgDFHnwk6u7pWY42E3c
namespace Eco.Plugins.DiscordBridge
{
    public class Bot
    {
        private DiscordClient _discordClient;
        public string CommandPrefix = "+";
        //public DiscordColor EmbedColor = DiscordColor.Gold;

        public void Init()
        {

        }

        public async Task SetupAsync()
        {
            //if (_discordClient == null) return;
            Logger.Debug("Bot initialized");
            _discordClient = new DiscordClient(new DiscordConfiguration
            {
                AutoReconnect = true,
                Token = DiscordBridge.Obj.DiscordPluginConfig.BotToken,
                TokenType = TokenType.Bot
            });

            if (DiscordBridge.Obj.DiscordPluginConfig.CommandPrefix != "")
            {
                CommandPrefix = DiscordBridge.Obj.DiscordPluginConfig.CommandPrefix;
            }
            await ConnectAsync();
        }

        public async Task<object> ConnectAsync()
        {
            try
            {
                await _discordClient.ConnectAsync();
                Logger.Debug("Connected to Discord.\n");
                Logger.Debug("Ping to discord: " + _discordClient.Ping);
                Logger.Debug("User" + _discordClient.CurrentUser);

                var game = new DiscordGame();
                game.Name = $"{CommandPrefix} für Kommandos";
                game.Details = $"Nutze meine Befehle mit {CommandPrefix} ";

                await _discordClient.UpdateStatusAsync(game, UserStatus.Online);

                _discordClient.MessageCreated += OnDiscordMessageCreateEvent;
            }
            catch (Exception e)
            {
                Logger.Error("Error connecting to discord: " + e.Message + "\n");
            }

            return null;
        }

        private async Task OnDiscordMessageCreateEvent(MessageCreateEventArgs messageArgs)
        {
            await OnMessageReceivedFromDiscord(messageArgs.Message);
        }

        public async Task OnMessageReceivedFromDiscord(DiscordMessage message)
        {
            if (!IsCommand(message)) return;
            if (message.Author == _discordClient.CurrentUser) return;
            Logger.Debug("================ Message incoming. Beepop! ================");

            Logger.Debug("Botname: " + _discordClient.CurrentUser);
            Logger.Debug("Message received from Discord on channel: " + message.Channel.Name);
            Logger.Debug("Message Author: " + message.Author.Username);
            Logger.Debug("Webhook message " + message.WebhookMessage);
            Logger.Debug("Message: " + message.Content);

            if (!message.Content.StartsWith(CommandPrefix)) { return; }
            if (message.WebhookMessage) { return; }

            Logger.Debug("Replying to " + message.Author.Username);
            //message.RespondAsync("pong!");

            if (!MatchCommandAndExecute(message))
            {
                message.RespondAsync($"Befehl `{CommandPrefix}{ExtractCommandFromMessage(message)}` ist nicht bekannt. Eine Liste von Befehlen bekommst du mit `+commands`.");
            }
            Logger.Debug("================ Message incoming. Ende! ================");
        }

        //[DiscordCommand("Fügt dem ECO-Server einen Benutzer hinzu")]
        //public void AddUser(DiscordMessage message)
        //{
        //    Eco.Gameplay.Players.UserManager.GetOrCreateUser("dslkfjlskdjflsdjfs", "", "test");
        //    message.RespondAsync("User angelegt");
        //}

        [DiscordCommand("Gibt einen Report über die Jobs aller Benutzer zurück.")]
        public void JobReport(DiscordMessage message, string user = "")
        {
            User ecoUser = null;
            if (user != "")
            {
                ecoUser = Eco.Gameplay.Players.UserManager.FindUserByName(user);
                if (ecoUser == null)
                {
                    message.RespondAsync($"Benutzer '{user}' wurde nicht gefunden.");
                }
            }
            Logger.Debug("================ JOB-REPORT ================");
            //Logger.Debug("User: "+user.Name);
            try
            {
                if (ecoUser == null)
                {

                    var builder = new DiscordEmbedBuilder()
                        .WithColor(DiscordColor.Gold)
                        .WithTitle($"** Job-Report **")
                        .WithDescription($"Hier siehst du die erlernten Berufe und Spezialitäten aller Spieler");
                    Eco.Gameplay.Players.UserManager.Users.ForEach(u =>
                    {

                        Logger.Debug("Report für " + u.Name);
                        var userName = u.Name;
                        var professions = new ProfessionInName().GetHighestProfession(u);

                        builder.AddField(userName, professions);
                    });
                    message.RespondAsync("", false, builder);
                }
                else
                {
                    var userName = ecoUser.Name;
                    var professions = new ProfessionInName().GetHighestProfession(ecoUser);

                    var builder = new DiscordEmbedBuilder()
                        .WithColor(DiscordColor.Gold)
                        .WithTitle($"** Job-Report für {userName} **")
                        .WithDescription($"Hier siehst du die erlernten Berufe und Spezialitäten von {userName}")
                        .AddField("Berufe", professions);
                    //.AddField("Spezialitäten", professions);

                    message.RespondAsync("", false, builder);
                }
                Logger.Debug("================ JOB-REPORT ENDE ================");
            }
            catch (Exception ex)
            {
                Logger.Error("Fehler: " + ex.Message);
            }
        }

        // [DiscordCommand("Lädt Mods neu")]
        // public void ReloadMods(DiscordMessage message)
        // {
        //     var rtc = new Eco.ModKit.RuntimeCompiler();            
        // }

        [DiscordCommand("Zeigt die aktuelle Marktlage an")]
        public void TradeReport(DiscordMessage message)
        {
            Logger.Debug("Command: tradereport");
            var stores = WorldObjectManager.All.SelectMany(o => o.Components.OfType<StoreComponent>());
            Logger.Debug($"Current Stores: {stores.Count()}");

            var builder = new DiscordEmbedBuilder()
              .WithColor(DiscordColor.Gold)
              .WithTitle($"** Markt-Report **")
              .WithDescription($"Hier siehst du einen Bericht über die aktuelle Lage am Markt");

            var ret = "";
            stores.ForEach(store =>
            {
                var sellOffers = store.SellOffers();
                var storeName = store.Parent.Name;
                var storeOwner = store.Parent.OwnerUser;

                var currencyName = store.Parent.Components.OfType<CreditComponent>().First().CurrencyName.StripHTML();

                Logger.Debug($"Current Store: {storeName}");
                Logger.Debug($"Current Store owner: {storeOwner}");
                Logger.Debug($"Current Store sell offers: {sellOffers.Count()}");

                sellOffers.ForEach(sellOffer =>
                {
                    var currentItem = sellOffer.SelectedItem.Item;
                    var itemName = currentItem.FriendlyNameLoc;
                    var itemPrice = sellOffer.Price.ToString("#.000");

                    ret += $"**{itemName}**: {itemPrice} {currencyName.Replace("**","")}\n";
                    //Logger.Debug(ret);
                });
                if (sellOffers.Count() > 0)
                {
                    builder.AddField($"**{storeName.StripHTML()}** (Besitzer: {storeOwner.Name.StripHTML()})", ret);
                }
                ret = "";
            });


            builder.Timestamp = DateTime.Now;
            // Logger.Debug(ret);
            try
            {
                var completed = message.RespondAsync("", false, builder).IsCompleted;
                if (completed)
                {
                    Logger.Debug("Message send!");
                }
            }
            catch (Exception ex)
            {
                Logger.Debug($"Fehler: {ex.Message} \n {ex.InnerException}");
            }

        }

        [DiscordCommand("Gibt einen Report über die Jobs aller Benutzer zurück.")]
        public void EnviromentReport(DiscordMessage message)
        {

            try
            {
                var ret = "";
                DateTime statsGenerated = new DateTime();

                var builder = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Gold)
                .WithTitle($"** Umwelt-Report **")
                .WithDescription($"Hier siehst du einen Bericht übr die aktuelle Umwelt für {DateTime.Now} im Vergleich zu {DateTime.Now.AddDays(-1)}");

                OrganismStats.GetPopulationStats().OrderBy(x => x.DisplayName).ForEach(s =>
                {

                    // if(s.DisplayName.Contains("Birch")){
                    TimeSpan timeSpan = DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0);
                    double unixVersion = timeSpan.TotalSeconds;
                    List<FloatStatEntry> i = s.GetGraphableData(unixVersion).ToList();
                    List<FloatStatEntry> iYesterday = s.GetGraphableData(DateTime.Now.AddDays(-1).Second).ToList();

                    var today = i.Last();
                    var yesterday = iYesterday.First();
                    statsGenerated = DateTime.Now.AddSeconds(today.TimeSeconds * -1);

                    var extinctionImminent = (yesterday.Value > today.Value ? true : false);
                    var difference = today.Value - yesterday.Value;
                    var trendIcon = (extinctionImminent ? "↘️" : "↗️");
                    
                    if(difference > 0){
                        trendIcon = "↗️";
                    }else if(difference < 0){
                        trendIcon = "↘️";
                    }else{
                        trendIcon = "↔";
                    }

                    ret += $"{s.DisplayName.Replace("Population", "")}: {today.Value} ({(difference > 0 ? "+" : "")}{difference}) \n";
                    // }
                });
                builder.AddField("Population", ret);
                builder.Timestamp = statsGenerated;
                // Logger.Debug(ret);
                var completed = message.RespondAsync("", false, builder).IsCompleted;
                if (completed)
                {
                    Logger.Debug("Message send!");
                }
            }
            catch (Exception ex)
            {
                Logger.Debug($"Fehler: {ex.Message} \n {ex.InnerException}");
            }
        }

        [DiscordCommand("Gibt alle verfügbaren Commands zurück")]
        public void Commands(DiscordMessage message)
        {
            var builder = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Blue)
                .WithTitle($"** Verfügbare Befehle **")
                .WithDescription($"Hier siehst du alle verfügbaren Befehle von mir.");

            foreach (var methodInfo in GetCommands())
            {
                var paramString = "";
                var attr = (DiscordCommand)typeof(Bot).GetMethod(methodInfo.Name)?.GetCustomAttributes(typeof(DiscordCommand), false).First();
                //Logger.Debug("Command: "+methodInfo.Name + " | Description: " + attr.Description);
                if (methodInfo.GetParameters().Length != -1)
                {
                    List<ParameterInfo> ps = methodInfo.GetParameters().ToList();
                    foreach (var parameterInfo in ps)
                    {
                        var p = parameterInfo;
                        if (p.Name != "message")
                        {
                            if (p.HasDefaultValue)
                            {
                                paramString += $"{paramString} *[{p.Name} (optional)]*";
                            }
                            else
                            {
                                paramString += $"{paramString} *{p.Name}*";
                            }
                        }

                    }
                }
                builder.AddField($"{CommandPrefix}{methodInfo.Name} {paramString}", $"{attr.Description}");
                Logger.Debug($"{CommandPrefix}{methodInfo.Name} {paramString} -> {attr.Description}");
            }
            message.RespondAsync("", false, builder);
        }

        public List<string> ParseCommandParams(DiscordMessage msg)
        {
            var body = msg.Content;
            Logger.Debug("ParseCommandParams(): " + msg.Content);
            var blankPosition = body.IndexOf(" ", StringComparison.Ordinal);
            Logger.Debug("ParseCommandParams(): Blankpos: " + blankPosition);
            if (blankPosition > 0)
            {
                var prms = body.Substring(blankPosition + 1).Split(' ').ToList();
                if (prms.Count > 1)
                {
                    Logger.Debug("ParseCommandParams(): Command has params:");
                    prms.ForEach(p =>
                    {
                        Logger.Debug("ParseCommandParams(): Params -> " + p);
                    });
                }

                return prms;
            }
            return new List<string>();
        }

        public bool MatchCommandAndExecute(DiscordMessage message)
        {
            try
            {
                if (!IsCommand(message)) return false;
                var cmd = ExtractCommandFromMessage(message);
                Logger.Debug("matchCommandAndExecute(): " + cmd);

                var method = GetCommand(cmd);
                if (method == null) return false;
                Logger.Debug("matchCommandAndExecute(): Got command -> " + method.Name);
                //var obj = Activator.CreateInstance(method.DeclaringType ?? throw new InvalidOperationException()); // Instantiate the class
                //Logger.Debug("matchCommandAndExecute(): Got Instance -> " + method.Name);

                //var commandParams = ParseCommandParams(message);

                //var methodParams = new object[] { message };

                //Logger.Debug("matchCommandAndExecute(): commandParams.count -> " + commandParams.Count);
                //Logger.Debug("matchCommandAndExecute(): methodParams.length -> " + methodParams.Length);

                List<object> methodParams = new List<object> { message };

                var commandParams = ParseCommandParams(message);
                if (commandParams.Count > 0)
                {
                    Logger.Debug("matchCommandAndExecute(): Command has params: " + commandParams.Count);
                    foreach (var commandParam in commandParams)
                    {
                        if (commandParam == "message") continue;
                        methodParams.AddOnce(commandParam);
                    }
                }

                //command.Invoke(obj, methodParams);
                InvokeMethod(method.Name, methodParams);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return false;
            }
        }

        public void InvokeMethod(string methodName, List<object> args)
        {
            Logger.Debug($"InvokeMethod(): {methodName}");
            foreach (var o in args.ToArray())
            {
                Logger.Debug($"InvokeMethod(): Param: {o.GetType()}");
            }
            // GetType().GetMethod(methodName)?.Invoke(this, args.ToArray());

            MethodInfo mi = GetType().GetMethod(methodName);

            List<object> ps = new List<object>();
            mi.GetParameters().ForEachIndex((arg, i) =>
            {
                if (args.ElementAtOrDefault(i) != null)
                {
                    ps.AddOnce(args.ElementAt(i));
                }else if(mi.GetParameters().ElementAtOrDefault(i).IsOptional){
                    ps.AddOnce(mi.GetParameters().ElementAtOrDefault(i).DefaultValue);
                }else{
                    var dm = (DiscordMessage)args.FirstOrDefault(x => x is DiscordMessage);
                    dm.RespondAsync($"Befehl '{args.ElementAt(i)}' ist nicht gesetzt, dieser wird aber benötigt!");
                }
            });
            mi.Invoke(this, ps.ToArray());
        }

        public static MethodInfo GetCommand(string name)
        {
            //Logger.Debug("GetCommand(): " + name);
            var cmd = GetCommands().First(x => string.Equals(x.Name.ToLower(), name.ToLower(), StringComparison.CurrentCultureIgnoreCase));
            //Logger.Debug("GetCommand(): Got command -> " + cmd.Name);
            return cmd;
        }

        public static IEnumerable<MethodInfo> GetCommands()
        {
            var commands = AppDomain.CurrentDomain.GetAssemblies() // Returns all currenlty loaded assemblies
                .SelectMany(x => x.GetTypes()) // returns all types defined in this assemblies
                .Where(x => x.IsClass) // only yields classes
                .SelectMany(x => x.GetMethods()) // returns all methods defined in those classes
                .Where(x => x.GetCustomAttributes(typeof(DiscordCommand), false).FirstOrDefault() != null); // returns only methods that have the InvokeAttribute

            return commands;
        }

        private bool IsCommand(DiscordMessage message)
        {
            var isCommand = message.Content.StartsWith(CommandPrefix);
            Logger.Debug("isCommand(): " + isCommand);
            return isCommand;
        }


        private string ExtractCommandFromMessage(DiscordMessage message)
        {
            var command = "";
            if (IsCommand(message))
            {
                var content = message.Content;
                command = content.IndexOf(" ", StringComparison.Ordinal) > 0
                    ? content.Substring(content.IndexOf(CommandPrefix, StringComparison.Ordinal) + 1, content.IndexOf(" ", StringComparison.Ordinal))
                    : content.Substring(content.IndexOf(CommandPrefix, StringComparison.Ordinal) + 1, content.Length - CommandPrefix.Length);
            }
            else
            {
                command = "";
            }

            //Logger.Debug("extractCommandFromMessage(): " + command);
            return command.Trim();
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class DiscordCommand : Attribute
    {
        public string Description { get; set; }

        public string User { get; set; }

        public DiscordCommand(string d)
        {
            Description = d;
        }
    }
}
