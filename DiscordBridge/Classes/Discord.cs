﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Windows.Input;
using Eco.Gameplay.Players;

namespace Eco.Plugins.DiscordBridge
{
    class DiscordWebhook
    {
        public string _webhook = "";

        private static readonly HttpClient client = new HttpClient();

        //public DiscordWebhook(string webhook)
        //{
        //    webhook = string.Format(webhookFormat, webhook);
        //}

        public DiscordWebhook(string webhook)
        {
            Logger.Debug("DiscordWebhook.ctor()");
            _webhook = webhook;
            Logger.Debug("Discordwebhook.Constructor(): Webhook: " + webhook);

#if DEBUG
           SendDiscordMessage("Webhook connected!", webhook);
#endif

        }

        public async void SendDiscordMessage(string message)
        {
            Logger.Debug("Discordwebhook.SendDiscordMessage(message): " + message);
            await SendDiscordMessage(message, _webhook);
        }

        public async System.Threading.Tasks.Task SendDiscordMessage(string message, string webhookUrl)
        {
            //Logger.Info("Discordwebhook.SendDiscordMessage(string message, string webhookUrl): " + message + " | " + webhookUrl);
            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "content", message }
            };

            if (_webhook.CheckURLValid())
            {
                Logger.Info("Discordwebhook.SendDiscordMessage: Valid url");
                var content = new FormUrlEncodedContent(values);

                var ret = await client.PostAsync(webhookUrl, content);
                Logger.Info("Discordwebhook.SendDiscordMessage: Status: "+ret.StatusCode.ToString());
                if (ret.IsSuccessStatusCode)
                {
                    Logger.Debug("Discordwebhook.SendDiscordMessage: Message send via hook");
                    
                }
                values.Clear();
            }
            else
            {
                Logger.Error(string.Format("{0} is not a valid discord webhook url.", webhookUrl));
            }

        }

       

    }
   
    
}
