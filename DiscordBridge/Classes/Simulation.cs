﻿using System.Collections.Generic;
using System.Linq;

namespace Eco.Plugins.DiscordBridge
{
    class Simulation
    {
        public class DiscordBridgeSimulation
        {
           
            public static List<Stats.PopulationStatManager> GetEnviromentStats()
            {
                return Eco.Stats.OrganismStats.GetPopulationStats().ToList();
            }
            public static string GetEnviromentStatsString(List<Stats.PopulationStatManager> stats)
            {
                string s = "";
                stats.ForEach(stat =>
                {
                    string line = "Type: " + stat.DisplayName + " | Units: " + stat.Unit + '\n';
                    s += line;
                    Logger.Info("GetEnviromentStatsString: " + line);

                });
                return s;
            }

            public static string GetEnviromentStatsString()
            {
                List<Stats.PopulationStatManager> stats = GetEnviromentStats();

                string s = "";
                stats.ForEach(stat =>
                {
                    string line = "Type: " + stat.DisplayName + " | Units: " + stat.Unit + '\n';
                    s += line;
                    Logger.Info("GetEnviromentStatsString: " + line);

                });
                return s;
            }
        }

       
    }
}


