﻿using Eco.Core.Utils;
using Eco.Gameplay.Players;
using Eco.Gameplay.Skills;
using Eco.Shared.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Eco.Plugins.DiscordBridge
{
    class ProfessionInName
    {
        public void Initialize()
        {
            Logger.Info("ProfessionInName.Initialize()");
            //changeEcoUserNames();
            //resetNames();
        }

        public void Run()
        {
            //while (true)
            //{
            //    User user = Eco.Gameplay.Players.UserManager.Users.First<User>(x => x.Name == "Disane");
            //    user.Name = user.Name + String.Format("[{0}]", user.PlayerObjective.Objective);
            //}

        }

        public void resetNames()
        {
            Eco.Gameplay.Players.UserManager.Users.ForEach(user =>
            {
                if (user.Name != "")
                {
                    //string objective = user.PlayerObjective.Objective.ToString();
                    //if (objective != "")
                    //{
                    Logger.Info("ProfessionInName.resetNames(): Changing name for " + user.Name);
                    string nameFormat = DiscordBridge.Obj.DiscordPluginConfig.UsernameFormat;
                    string cleanName = user.Name.Substring(user.Name.LastIndexOf("]") + 1).Trim();

                    if (GetHighestProfession(user) != "")
                    {
                        ChangeUsername(user, cleanName);
                    }

                    Logger.Info("ProfessionInName.resetNames(): \nUsername: " + user.Name + " | Clean name: " + cleanName);
                    //}
                }
            });
        }

        public void changeEcoUserNames()
        {
            Logger.Info("ProfessionInName.changeEcoUserNames()");
            Eco.Gameplay.Players.UserManager.Users.ForEach(user =>
            {
                if (user.Name != "")
                {
                    //string objective = user.PlayerObjective.Objective.ToString();
                    //if (objective != "")
                    //{
                    Logger.Info("ProfessionInName.changeEcoUserNames(): Changing name for "+ user.Name);
                    string nameFormat = DiscordBridge.Obj.DiscordPluginConfig.UsernameFormat;
                    string cleanName = user.Name.Substring(user.Name.LastIndexOf("]") + 1).Trim();
                    string newName = nameFormat.Replace("{username}", cleanName).Replace("{profession}", GetHighestProfession(user));

                    if(GetHighestProfession(user) != "")
                    {
                        ChangeUsername(user, newName);
                    }

                    Logger.Info("ProfessionInName.Initialize(): \nUsername: " + user.Name + " | Clean name: " + cleanName + " | New name: " + newName);
                    //}
                }
            });
        }

        private void ChangeUsername(User user, string newName)
        {
            //if (!user.Name.Contains(newName))
            //{
                string previousName = user.Name;

                typeof(User).GetFields(BindingFlags.NonPublic | BindingFlags.Instance).First(x => x.Name.ContainsCaseInsensitive("Name")).SetValue(user, newName);
                ThreadSafeDictionary<string, User> users = (typeof(UserManager).GetFields(BindingFlags.NonPublic | BindingFlags.Static).First(x => x.Name.ContainsCaseInsensitive("UsersByDisplayName")).GetValue(UserManager.Obj) as ThreadSafeDictionary<string, User>);
                users.Remove(previousName);
                users.Add(newName, user);
            //}
            //else
            //{
            //    Logger.Info("Name already has prefix " + newName);
            //}

        }

        public string GetHighestProfession(User user)
        {
            Logger.Info("GetHighestProfession(): "+user.Name);
            IList<Profession> professions = new List<Profession>();
            IList<Skill> trainedSkills = new List<Skill>();
            //.Where(x => x.IsRoot)
            user.Skillset.Skills.ForEach(skill =>
            {
                int skillpoints = 0;
                string professionName = skill.RootSkillTree.StaticSkill.ToString();

                ThreadSafeList<int> pointsSpent = skill.PointsSpent;
                pointsSpent.ForEach(points =>
                {
                    skillpoints += points;
                });

                if (skillpoints > 0)
                {
                    trainedSkills.AddOnce(skill);
                    if (professions.FirstIndex(p => p.Name.Contains(professionName)) == -1)
                    {
                        professions.AddOnce(entry: new Profession()
                        {
                            Name = professionName
                        });
                    }

                }
            });
            if(professions.Count() > 0) return string.Join(", ", professions.Select(x => x.Name));
            return "Kein Beruf ermittelt";
        }
    }

    public class Profession
    {
        public string Name { get; set; }
    }
}
