﻿param (
  [bool]$ui = $False
)

Write-Host "--- Eco-Server Debugger ---" -ForegroundColor Blue
Write-Host "--- UI: $ui ---"

$modName = "DiscordBridge"
$ecoServerPath = "E:\temp\games\steamapps\common\Eco Server\"
$modFolder = "$ecoServerPath\mods\$modName"

$ecoServer = Get-Process -Name EcoServer -ErrorAction SilentlyContinue
if ($ecoServer) {
  # try gracefully first
  $ecoServer.CloseMainWindow()
  # kill after five seconds
  Sleep 5
  if (!$ecoServer.HasExited) {
    $ecoServer | Stop-Process -Force
  }
}

If(!(test-path $modFolder))
{
      New-Item -ItemType Directory -Force -Path $modFolder
}
Remove-Item "$modFolder\*.*"
Copy-Item -Path "$PSScriptRoot\bin\Debug\*" -Destination $modFolder -Force -Include *.dll -Exclude *ECO*
# Start-Process -FilePath "$ecoServerPath\EcoServer.exe"

# Process starten und in die Console umleiten
$params = "-nogui"
$ecoServerAppPath = "& '$ecoServerPath\EcoServer.exe'"

if(!$ui){
  $ecoServerAppPath += " $params"
}
Write-Host "Starting $ecoServerAppPath"
Invoke-Expression $ecoServerAppPath -OutVariable output -ErrorVariable errors
Write-Host $output