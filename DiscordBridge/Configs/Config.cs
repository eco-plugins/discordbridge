﻿using Eco.Core.Utils;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace Eco.Plugins.DiscordBridge
{
    public class DiscordBridgeConfig
    {
        internal ThreadSafeAction<string> OnConfigChanged { get; set; } = new ThreadSafeAction<string>();

        private string _messageLogPath = "";

        private static string _usernameFormat = "";
        private static string _usernameFormatDefault = "[{profession}] {username}";
        private static string _webhookFormat = "https://discordapp.com/api/webhooks/{0}";

        private static string _discordBotToken = "";

        private static string _botCommandPrefix = "+";
        

        public DiscordBridgeConfig()
        {
            _messageLogPath = System.Reflection.Assembly.GetAssembly(typeof(DiscordBridge)).Location;
            if (_usernameFormat == "")
            {
                _usernameFormat = _usernameFormatDefault;
            }
        }

        

        private string _webhook = "";
        [Description("Discord webhook id of the channel you want to propagate your ingame chat."), Category("Discord")]
        public string Webhook
        {
            get
            {
                return _webhook;
            }
            set
            {
                if (_webhook != value)
                {
                    _webhook = value;
                    OnConfigChanged.Invoke(value);
                }
            }
        }

        [Description("Token of your Bot which interacts with ECO."), Category("Discord")]
        public string BotToken
        {
            get
            {
                return _discordBotToken;
            }
            set
            {
                if (_discordBotToken != value)
                {
                    _discordBotToken = value;
                    OnConfigChanged.Invoke(value);
                }
            }
        }

        [Description("Prefix for activating a bot command"), Category("Discord")]
        public string CommandPrefix
        {
            get
            {
                return _botCommandPrefix;
            }
            set
            {
                if (_botCommandPrefix != value)
                {
                    _botCommandPrefix = value;
                    OnConfigChanged.Invoke(value);
                }
            }
        }

        [Description("Parsed discord webhook url"), Category("Discord")]
        public string WebhookUrl
        {
            get
            {
                return string.Format(_webhookFormat, Webhook);
            }
        }


        [Description("Format of the username and the profession. Default is '[{profession}] {username}'"), Category("Professions")]
        public string UsernameFormat
        {
            get
            {
                return _usernameFormat;
            }
            set
            {
                if (_usernameFormat != value)
                {
                    _usernameFormat = value;
                    OnConfigChanged.Invoke(value);
                }
            }
        }

        
        public List<string> _whiteListChannels = new List<string>();
        [Description("Whitelisted channels"), Category("Discord")]
        public List<string> WhiteListChannels
        {
            get
            {
                return _whiteListChannels;
            }
            set
            {
                _whiteListChannels = value;
            }
        }

        
         public List<string> _blacklistChannels = new List<string>();
        [Description("Whitelisted channels"), Category("Discord")]
        public List<string> BlacklistChannels
        {
            get
            {
                return _blacklistChannels;
            }
            set
            {
                _blacklistChannels = value;
            }
        }


        [Description("Logging path for unstripped messages"), Category("Messages")]
        public string MessageLogPath
        {
            get
            {
                string path = Path.Combine(Helper.GetDLLFolder(), "chatlog.txt");
                //Directory.CreateDirectory(Path.GetDirectoryName(path));
                //File.CreateText(path);

                //return Path.Combine(Helper.GetDLLFolder(),"chatlog.txt");
                return path;
            }
            //set
            //{
            //    if (_messageLogPath != value)
            //    {
            //        _messageLogPath = value;
            //        OnConfigChanged.Invoke(value);
            //    }
            //}
        }
    }

    
}
